package org.itstep.qa.lesson;

import java.util.Random;

public class ChessGame {
    public static void main(String[] args) {
        int[] array = new int[5];
        Random random = new Random();
        for (int i = 0; i<array.length;i++){
            array[i]=random.nextInt(100);
        }
        System.out.println("Таблица игроков и их очки:");
        for(int j=0;j<array.length; j++){
            System.out.println("a[" + j + "] = " + array[j]);
        }

        //ищем победителя
        int maxScore = 0;
        int maxNumber = 0;
        for (int k=0; k<array.length; k++){
            maxScore = array[k];
            for(int m=0; m<array.length; m++){
                if (array[m]>maxScore){
                    maxScore = array[m];
                    maxNumber = m;
                }
            }
        }
        System.out.println();
        System.out.println("Победил игрок a[" + maxNumber + "]," +
                " набравший " + maxScore + " очков.");

        //ищем проигравшего
        int minScore = 0;
        int minNumber = 0;
        for (int k=0; k<array.length; k++){
            minScore = array[k];
            for(int m=0; m<array.length; m++){
                if (array[m]<minScore){
                    minScore = array[m];
                    minNumber = m;
                }
            }
        }
        System.out.println();
        System.out.println("Самый слабый игрок a[" + minNumber + "]," +
                " набравший " + minScore + " очков.");
        System.out.println();
        System.out.println("Разница между победителем и самым слабым " +
                " игроками составила: " + (maxScore-minScore) + " очков.");
    }
}

